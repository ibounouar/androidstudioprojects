package com.eilco.android.td2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity  {
    private EditText identifiant;
    private EditText pwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        identifiant = (EditText) findViewById(R.id.identifiant);
        pwd = (EditText) findViewById(R.id.pwd);
    }

    public void login(View v) {
        Intent intent = new Intent(this, LoginActivity.class);
        String id = identifiant.getText().toString();
        String pd = pwd.getText().toString();
        intent.putExtra( "identifiant", id);
        intent.putExtra( "pwd", pd);
        startActivity(intent);
    }
}
