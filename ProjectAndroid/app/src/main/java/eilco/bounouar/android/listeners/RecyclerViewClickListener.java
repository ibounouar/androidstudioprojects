package eilco.bounouar.android.listeners;

import android.view.View;

import eilco.bounouar.android.model.Movie;

public interface RecyclerViewClickListener {
    void recyclerViewListClicked(View v, Movie currentMovie);
}
