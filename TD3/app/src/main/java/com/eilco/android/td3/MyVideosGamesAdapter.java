package com.eilco.android.td3;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class MyVideosGamesAdapter extends RecyclerView.Adapter<MyViewHolder> {
    List<JeuVideo> mesJeux;

    MyVideosGamesAdapter(List<JeuVideo> mesJeux) {
        this.mesJeux = mesJeux;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.jeu_video_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.display(mesJeux.get(position));
    }

    @Override
    public int getItemCount() {
        return mesJeux.size();
    }
}
