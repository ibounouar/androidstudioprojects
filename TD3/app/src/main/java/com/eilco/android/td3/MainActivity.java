package com.eilco.android.td3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private List<JeuVideo> mesJeux;
    private MyVideosGamesAdapter monAdapter;

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView)findViewById(R.id.myRecyclerView);

        mesJeux = new ArrayList<>();

        //ajouter des nouveaux jeux vidéo dans la liste
        mesJeux.add(new JeuVideo("Forza Horizon 3", 49.90F));
        mesJeux.add(new JeuVideo("Forza Motorsport 5", 20.00F));
        mesJeux.add(new JeuVideo("Asseto Corsa", 22.00F));
        mesJeux.add(new JeuVideo("Gran Theft Auto 5", 49.90F));

        monAdapter = new MyVideosGamesAdapter(mesJeux);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecyclerView.setAdapter(monAdapter);
    }
}
