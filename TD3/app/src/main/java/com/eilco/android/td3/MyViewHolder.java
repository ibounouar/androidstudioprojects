package com.eilco.android.td3;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class MyViewHolder extends RecyclerView.ViewHolder {
    private TextView mNameTV;
    private TextView mPriceTV;

    MyViewHolder(View itemView) {
        super(itemView);

        mNameTV = (TextView)itemView.findViewById(R.id.name);
        mPriceTV = (TextView)itemView.findViewById(R.id.price);
    }

    void display(JeuVideo jeuVideo) {
        mNameTV.setText(jeuVideo.getName());
        mPriceTV.setText(jeuVideo.getPrice() + "€");
    }
}
